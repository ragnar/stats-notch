
#ifndef _RAM_H
#define _RAM_H

#define BYTES2MB(x)	((float)(x) / 1048576.0f)
#define BYTES2GB(x) ((float)(x) / 1073741824.0f)

/* ram stats, see https://wiki.freebsd.org/Memory for ref.
 * tl;dr:
 * wired	- mega used, won't be given back any time soon
 * active	- used by processes
 * laundry	- inactive but dirty memory that has to be cleaned before freed
 * inactive	- malmost free (but can possible be dirty)
 * free		- go right ahead and take it
 */
struct ram {
	unsigned long wire,
				  active,
				  laundry,
				  inactive,
				  free,
				  tot_mem;
	int pagesz;
};

void ram_info(struct ram *r);


#endif

