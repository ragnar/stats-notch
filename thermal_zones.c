
#include <sys/types.h>
#include <sys/sysctl.h>
#include <err.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

// this one's also in cpu.c.. that's fine.
#define TEMP2C(x) (((float)(x)/10.0f) - 273.15f)

// TODO
// - figure out how to find out the number of thermal zones (and preferably
//   some additional info on them)
// - make a similar structure as cpu.c

float thermal_zone_temp(int tz) {
	int mib_len, t, error;
	size_t len = sizeof(t);
	char *ascii_mib;
	
	mib_len = asprintf(&ascii_mib, "hw.acpi.thermal.tz%u.temperature", tz);
	if (mib_len < 0) err(ENOMEM, "asprintf");
	error = sysctlbyname(ascii_mib, &t, &len, NULL, 0);
	free(ascii_mib);
	if (error < 0) err(errno, "sysctlbyname");
	return TEMP2C(t);
}

