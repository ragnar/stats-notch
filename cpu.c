// TODO:
// - look up if logical cores can traced back to physical ones and in that case
//   make that pretty layout with the die and such for the gui (maybe also tui)
// - turn on the sshing on the workstation so you can test amd-stuff
// - split this cpu_info into an intel and an amd branch (possibly also struct cpu_core)

#include <stdlib.h>
#include <sys/types.h>
#include <sys/resource.h>	// declares CPUSTATES
#include <sys/systm.h>		// do_cpuid (and datatypes to be used with it)
#include <sys/sysctl.h>
#include <err.h>
#include <errno.h>
#include <stdio.h>

#include "cpu.h"

// this could be done in a nicer way
#define IS_AMD(ebx, ecx, edx)	((ebx) == 0x68747541 && \
								 (ecx) == 0x444D4163 && \
								 (edx) == 0x69746E65)
#define IS_INTEL(ebx, ecx, edx)	((ebx) == 0x756e6547 && \
								 (ecx) == 0x6c65746e && \
								 (edx) == 0x49656e69)

// temperatures are returned in 10*K, we always return C and let the caller
// deal with any further unit conversion
#define TEMP2C(x) (((float)(x)/10.0f) - 273.15f)

// TODO: check if cp_times even exist on single core systems
struct mib_cache {
	int dev_cpu[2],
		_cp_time[2],
		_cp_times[2];
	int *cp_t;	// to avoid branching every cpu_info call this one points to
				// cp_time if no_cores == 1 and cp_times otherwise
	size_t sz_cp_t; // this one holds the size needed for the cp_t buf
};
static inline void mib_cache_init(struct mib_cache *mc) {
	size_t ln = 2;
	sysctlnametomib("dev.cpu", mc->dev_cpu, &ln);
	ln = 2;
	sysctlnametomib("kern.cp_time", mc->_cp_time, &ln);
	ln = 2;
	sysctlnametomib("kern.cp_times", mc->_cp_times, &ln);
}

static int no_cores() {
	static int n;
	int mib[2];
	size_t len;

	if (n == 0) {
		mib[0] = CTL_HW;
		mib[1] = HW_NCPU;
		len = sizeof(int);
		if (sysctl(mib, 2, &n, &len, NULL, 0) < 0) {
			// TODO log error
			err(errno, "hw.ncpu");
		}
	}
	return n;
}

static char *cpu_model() {
	char *nm;
	int mib[2] = {CTL_HW, HW_MODEL};
	size_t len;

	if (sysctl(mib, 2, NULL, &len, NULL, 0) < 0) {
		// TODO log error
		err(errno, "Failed to query hw.model size");
	}
	nm = malloc(len * sizeof(char));
	if (sysctl(mib, 2, nm, &len, NULL, 0) < 0) {
		// TODO log error
		err(errno, "Failed to query hw.model");
	}
	return nm;
}

static enum CPU_MAKE cpu_make() {
// see <machine/cpufunc.h> for more info
	static enum CPU_MAKE m;
	u_int regs[4];
	if (m == 0) {
		do_cpuid(0, regs); // code 0 is manufacturer id
		if (IS_AMD(regs[1], regs[2], regs[3])) m = CPU_AMD;
		else if (IS_INTEL(regs[1], regs[2], regs[3])) m = CPU_INTEL;
		else m = CPU_UNKNOWN;
	}
	return m;
}

static inline int int_sc(int *mib, u_int mib_len) {
	int res;
	size_t len = sizeof(res);
	if (sysctl(mib, mib_len, &res, &len, NULL, 0) < 0) {
		// TODO log error
		err(errno, "sysctl call failed");
	}
	return res;
}

// TODO
// - make amd version
static struct cpu *make_cpu_struct(enum CPU_MAKE make) {
	struct cpu *c;
	char *nm, *model;
	int nmln, i, mib[5];
	size_t mibln;

	if (make == CPU_UNKNOWN) {
		// TODO log error
		fprintf(stderr, "Unknown cpu\n");
		return NULL;
	}

	model = cpu_model();
	// TODO bail out if the above fails
	
	// calloc so we can lazily just call free_cpu_struct if something fails
	c = calloc(1, sizeof(struct cpu));
	if (c == NULL) {
		free(model);
		// TODO log error
		err(errno, "struct cpu calloc");
	}
	c->no_cores = no_cores();
	// calloc to ensure that we read the tjmax value the first time
	c->cores = calloc((size_t)c->no_cores, sizeof(struct cpu_core));
	if (c->cores == NULL) {
		free_cpu_struct(c);
		// TODO log error
		err(errno, "struct core calloc");
	}
	c->_cp_times = calloc((size_t)2*CPUSTATES*c->no_cores, sizeof(long));
	if (c->_cp_times == NULL)  {
		free_cpu_struct(c);
		// TODO log error
		err(errno, "struct cache calloc");
	}
	c->make = make;
	c->model = model;

	// and then per core caches..
	// for that we first need a buf to hold the different sysctl names
	nmln = asprintf(&nm, "dev.cpu.%u.coretemp.tjmax", c->no_cores-1);
	if (nmln < 0) {
		// TODO log error
		free_cpu_struct(c);
		err(ENOMEM, "asprintf");
	}
	// short notes on this cache:
	// - unfortunately the core numbers' mib value is not their index (like
	//   pids)
	// - the subvalues (coretemp, tjmax, temperature, and freq) also differ in
	//   mib values between cores.
	for (i = 0; i < c->no_cores; i++) {
		// this first one gets us the cpu number, we also read and set the
		// value of tjmax here
		snprintf(nm, nmln+1, "dev.cpu.%u.coretemp.tjmax", i);
		mibln = sizeof(mib)/sizeof(int);
		sysctlnametomib(nm, mib, &mibln);
		c->cores[i]._mib_cache.core_no = mib[2];
		c->cores[i].throttle_temp = TEMP2C(int_sc(mib, mibln));
		// then we fetch the temp
		snprintf(nm, nmln+1, "dev.cpu.%u.temperature", i);
		mibln = sizeof(mib)/sizeof(int);
		sysctlnametomib(nm, mib, &mibln);
		c->cores[i]._mib_cache.temp = mib[3];
		// and finally the freq
		snprintf(nm, nmln+1, "dev.cpu.%u.freq", i);
		mibln = sizeof(mib)/sizeof(int);
		sysctlnametomib(nm, mib, &mibln);
		c->cores[i]._mib_cache.freq = mib[3];
	}
	free(nm);
	
	return c;
}

static inline void util_with_swap(long *new, long *old, struct cpu_utilization *dst) {
// see /usr/src/usr.bin/top for ref
	int i;
	unsigned long diffs[CPUSTATES], tot_diff = 0;
	for (i = 0; i < CPUSTATES; i++) {
		diffs[i] = new[i] - old[i];
		tot_diff += diffs[i];
		old[i] = new[i];
	}
	if (tot_diff == 0) tot_diff = 1;
	dst->user		= (float)diffs[CP_USER] / tot_diff;
	dst->nice		= (float)diffs[CP_NICE] / tot_diff;
	dst->system		= (float)diffs[CP_SYS]  / tot_diff;
	dst->interrupt	= (float)diffs[CP_INTR] / tot_diff;
	dst->idle		= (float)diffs[CP_IDLE] / tot_diff;
}

struct cpu *cpu_info(struct cpu *c) {
	int mib[5], i;
	size_t sz_cp_t;
	static struct mib_cache mc;
	enum CPU_MAKE make = cpu_make();

	// TODO: branch based on cpu make

	if (c == NULL) {
		c = make_cpu_struct(make);
		// TODO: deal with c being NULL here
	
		// populate the mib cache
		if (mc.sz_cp_t == 0) {
			mib_cache_init(&mc);
			mc.cp_t = (c->no_cores > 1) ? mc._cp_times : mc._cp_time;
			mc.sz_cp_t = sizeof(long)*CPUSTATES*c->no_cores;
		}
	}
	
	sz_cp_t = mc.sz_cp_t;
	if (sysctl(mc.cp_t, 2, c->_cp_times, &sz_cp_t, NULL, 0) < 0) {
		// TODO log error
		err(errno, "cp_times sysctl call failed");
	}
	// TODO: what if sz_cp_t is not == mc.sz_cp_t?

	mib[0] = mc.dev_cpu[0];
	mib[1] = mc.dev_cpu[1];
	for (i = 0; i < c->no_cores; i++) {
		mib[2] = c->cores[i]._mib_cache.core_no;
		mib[3] = c->cores[i]._mib_cache.temp;
		c->cores[i].temp = TEMP2C(int_sc(mib, 4));
		mib[3] = c->cores[i]._mib_cache.freq;
		c->cores[i].freq = int_sc(mib, 4);
		util_with_swap(&(c->_cp_times[i*CPUSTATES]),
					   &(c->_cp_times[(c->no_cores+i)*CPUSTATES]),
					   &(c->cores[i].util));
	}

	return c;
}

void free_cpu_struct(struct cpu *c) {
	free(c->_cp_times);
	free(c->model);
	free(c->cores);
	free(c);
}

