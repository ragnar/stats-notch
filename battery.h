
#ifndef _BAT_H
#define _BAT_H

#include <stdint.h>
#include <stdbool.h>
#include <dev/acpica/acpiio.h>

/* should be a acpi define, but doesn't appear to be */
#define UNKNOWN_CAP		0xffffffff
#define UNKNOWN_VOLTAGE	0xffffffff

/* not sure if these are defined or documented somewhere */
#define UKNOOWN_PERCENT	-1
#define UNKNOWN_TIME	-1
#define UNKNOWN_RATE	-1

enum battery_state {
	HIGH					= 0x0000000f,
	CHARGING				= 0x000000f0,
	DISCHARCHING			= 0x00000f00,
	CRITICAL				= 0x0000f000,
	CRITICAL_CHARGING		= 0x0000f0f0,
	CRITICAL_DISCHARGING	= 0x0000ff00,
	UNKNOWN					= 0
};

struct battery_extras {
	const char *base_unit;
	uint32_t design_cap,		// mAh or mWh
			 last_full_cap,		// mAh or mWh
			 design_voltage,	// mV
			 current_voltage,	// mV
			 cycles;
	char type[ACPI_CMBAT_MAXSTRLEN]; 
};

struct battery {
	int num, percent, h_left, m_left, rate; // rate given in mW or mA
	enum battery_state state;
	struct battery_extras extra;
	uint8_t valid_read;
};

void battery_info(struct battery *bat, bool get_extras);


#endif

