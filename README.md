# stats-notch
a status notch for wayland on freebsd (stable/13.1 and above)

## status
not usable yet, i have just started. one goal is to keep the different "modules" fairly self contained such that they can easily be ripped out and reused in other projects.

## requirements
hopefully very few.. thermals requires core/amdtemp to be loaded. currently it only works on amd64 systems.. this __might__ be fixed at some point.

