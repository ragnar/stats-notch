
#ifndef _CPU_H
#define _CPU_H

enum CPU_MAKE { CPU_AMD = 1, CPU_INTEL, CPU_UNKNOWN };

struct cpu_utilization {
	float user,		// user processes
		  nice,		// processes with a nice value > 0
		  system,
		  interrupt,
		  idle;
};

struct cpu_core {
	float temp,				// core temp in deg celsius
		  throttle_temp;	// core throttle temp (tj max) in deg celsius
	int freq;				// core frequency in MHz
	struct cpu_utilization util;	// core utils in percent
	// note that throttle_temp is only read on startup (not updated by calling
	// cpu_info()), so if your fiddling with them at runtime (if that is even
	// possible) you're gonna have to restart stat[-notch]
	
	// caching is used to go more fast
	struct { int core_no, temp, freq; } _mib_cache;
};

struct cpu {
	enum CPU_MAKE make;		// intel or AMD
	char *model;			// full cpu name
	int no_cores;			// number of cores
	struct cpu_core *cores;	// per-core info (see struct cpu_core)
	long *_cp_times;		// cache for core utilization calcs
};

// if c == NULL, a struct is allocated for you. intended usage is:
// struct cpu *c = cpu_info(NULL);
// printf("cpu stuff: ...", c->...);
// //... set up whatever polling routine you want ...
// cpu_info(c);
// printf("cpu stuff: ...", c->...);
// //... and then in the cleanup ...
// free_cpu_struct(c);
//
// note that the first cpu_info(NULL) call takes much longer than the
// subsequent cpu_info(c) calls to return.
struct cpu *cpu_info(struct cpu *c);
void free_cpu_struct(struct cpu *c);

#endif

