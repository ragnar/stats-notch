/*
 * getting more info about ram - brand, MT/s, etc - afaik requires
 * reading rights to /dev/mem.. whuch like, noone should have 
 */

#include <stdlib.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#include <err.h>
#include <errno.h>

#include "ram.h"

#define ERR_STR_PTRN "Failed to query %s"

// TODO mush these ones together to get like a template thing instead of these
//      three dummies
static inline unsigned long physmem() {
// assumes hw.physmem returns 8 bytes
// TODO (low prio): look up and handle properly
	static unsigned long sz;
	int mib[2];
	size_t len;

	// only make the call if needed (doubt it'll run on systems with hot
	// swappable ram)
	if (sz == 0) {
		mib[0] = CTL_HW;
		mib[1] = HW_PHYSMEM;
		len = sizeof(unsigned long);
		if (sysctl(mib, 2, &sz, &len, NULL, 0)  < 0) {
			// TODO log error
			err(errno, ERR_STR_PTRN, "hw.physmem");
		}
	}
	return sz;
}

static inline int pagesz() {
	static int sz;
	int mib[2];
	size_t len;

	if (sz == 0) {
		mib[0] = CTL_HW;
		mib[1] = HW_PAGESIZE;
		len = sizeof(int);
		if (sysctl(mib, 2, &sz, &len, NULL, 0) < 0) {
			// TODO log error
			err(errno, ERR_STR_PTRN, "hw.pagesize");
		}
	}
	return sz;
}

static inline int sc(int *mib, u_int mib_len) {
	int res;
	size_t len = sizeof(res);
	if (sysctl(mib, mib_len, &res, &len, NULL, 0) < 0) {
		// TODO log error
		err(errno, "sysctl failed");
	}
	return res;
}

// to make the ram_info less wordy:
#define RAM_SC_CACHE(nm, mc, ln)									\
	ln = 4;															\
	sysctlnametomib("vm.stats.vm.v_" #nm "_count", mc.mib, &ln);	\
	mc.nm = mc.mib[3];

#define RAM_SET(nm, mc, dst)								\
	mc.mib[3] = mc.nm;										\
	dst->nm = (unsigned long)dst->pagesz * sc(mc.mib, 4);

void ram_info(struct ram *r) {
	size_t ln;
	static struct {
		int mib[4], wire, active, laundry, inactive, free;
	} mc;

	if (mc.free == 0) {
		RAM_SC_CACHE(wire, mc, ln);
		RAM_SC_CACHE(active, mc, ln);
		RAM_SC_CACHE(laundry, mc, ln);
		RAM_SC_CACHE(inactive, mc, ln);
		RAM_SC_CACHE(free, mc, ln);
	}
	// TODO:
	// - maybe don't update physmem and pagesz unless asked
	r->tot_mem	= physmem();
	r->pagesz	= pagesz();
	RAM_SET(wire, mc, r);
	RAM_SET(active, mc, r);
	RAM_SET(laundry, mc, r);
	RAM_SET(inactive, mc, r);
	RAM_SET(free, mc, r);
}


