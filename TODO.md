## overview
- [ ] __datetime__
	- [ ] option: format [%m-%d %H:%M] (see strftime)

- [ ] __battery__
	- [ ] option: battery number [0]
	- [ ] symbols + text in notch
	- [x] fetch battery info
		- [ ] return error instead of bailing out
		- [ ] logging and error handling
	- [ ] warnings
	    - [ ] high discharge rate when on battery
	    - [ ] lobat when on battery
	- [ ] window showing those stats

- [ ] __cpu__ (relies on coretemp being loaded)
	- [ ] options:
		- [ ] thermal zone index [0] for icon temp (see hw.acpi.thermal)
		- [ ] temperature unit
	- [ ] symbol + text in notch
	- [x] fetch thermal and frequency info
		- [ ] return error instead of bailing out
		- [ ] logging and error handling
	- [ ] warnings
	    - [ ] high temps 
	- [ ] window showing dev.cpu.0-N.temperature distributed over die (if you can find such a thing, otherwise just list the temperatures)

- [ ] __wifi__
	- [ ] option: scan-on-right/alt/ctrl-click [on]
	- [ ] symbols
	- [ ] fetch wifi info
	- [ ] window:
		- [ ] info about current connection
		- [ ] buttons (+functions of course):
			- [ ] disconnect
			- [ ] scan
			- [ ] turn off wifi (if doable)
		- [ ] scan window (see macos)
	- [ ] warnings
	    - [ ] lost connection maybe?
	    - [ ] no interwebs connection

- [ ] __bluetooth__
	- [ ] option: scan-on-right/alt/ctrl-click [on]
	- [ ] symbols
	- [ ] window:
		- [ ] info about current state/connections
		- [ ] buttons:
			- [ ] scan for devs
			- [ ] disconnect (for each connection)
			- [ ] turn off

- [ ] __backlight__
	- [ ] fetch info
	- [ ] window with slider

- [ ] __ram usage__
	- [x] fetch info
		- [ ] fix the structure of the code.. pretty poopy now.
		- [ ] logging and error handling
		- [ ] swapping
	- [ ] warnings
	    - [ ] too little free/too little available
	    - [ ] swapping


- [ ] __power menu__ (maybe)
	- [ ] turn off and whatnot..

- [ ] __docs__
	- [ ] proper readme.md
	- [ ] man page
	- [ ] possibly wiki
	
- [ ] __volume__
	- [ ] since libmixer won't likely be ported to stable/13 but the kernel patches are see if you can't just extract the volume adjusting stuff for the active output from it..
	- [ ] options: none
	- [ ] symbols
	- [ ] fetch info
	- [ ] window: just a slider

- [ ] __fans__ (possibly)
	- [ ] requires a device driver..

- [ ] __disk space__
    - [ ] show free/used space of disk (check /usr/src/bin/df)

- [ ] __pkg updates__
    - [ ] should just show if and which packages can be updated

## v0.1 just cli/tui - so only stats, no notch
- [ ] cache all the mibs and then use that instead of the strings
- [ ] all of the non-interactive parts in the overview (except the windows)
- [ ] code cleanup.. it looks pretty crappy atm, especially cpu.c, the structure of which should also be fixed
- [ ] argument parsing
- [ ] output modes
	- [ ] simple
	- [ ] extended (prettyprint)
	- [ ] parseable by scripts

## v0.2 notch
- [ ] all of the non-interactive parts above, but now in GUI!
- [ ] notch window (see layer shell protocol)
- [ ] wayland stuff.. this point is going to grow =)



