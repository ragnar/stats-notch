#include "battery.h"
#include <unistd.h>		// open, close, etc
#include <fcntl.h>		// O_RDONLY, etc
#include <sysexits.h>	// EX_*
#include <sys/ioctl.h>
#include <string.h>

// these puppers will be replaced once we get a proper logger going
#include <stdio.h>
#include <err.h>		// err provide nice error messages 

#define	ACPIDEV	"/dev/acpi"

// TODO
// - get_battery_list

void battery_info(struct battery *bat, bool get_extras) {
// see /usr/src/usr.sbin/acpiconf for ref

// NOTE: not getting extras saves two ioctls, however extras are required to
// determine how long is left until fully charged if state & CHARGING
// (otherwise h/m_left will just be UNKNOWN_TIME)
	int acpifd = -1;
	union acpi_battery_ioctl_arg batio;
	uint32_t mins2full;

	bat->valid_read = 0;

	if (bat->num < 0 || bat->num > 64) {
		// TODO log error
		fprintf(stderr, "battery num out of bounds (%i).\n", bat->num);
		goto fail_return;
	}

	acpifd = open(ACPIDEV, O_RDONLY); 
	if (acpifd == -1) {
		// TODO log error
		err(EX_OSFILE, ACPIDEV);
		goto fail_return;
	}
	
	batio.unit = bat->num;

	// first populate the basics (battinfo)
	if (ioctl(acpifd, ACPIIO_BATT_GET_BATTINFO, &batio) == -1) {
		// TODO log error
		err(EX_IOERR, "get battery user info (%d) failed", bat->num);
		goto fail_return;
	} else if (batio.battinfo.state == ACPI_BATT_STAT_NOT_PRESENT) {
		// TODO log error
		fprintf(stderr, "Battery %i not present according to battinfo.\n",
				bat->num);
		goto fail_return;
	}
	
	// for now unknown values are just fine..
	bat->percent = batio.battinfo.cap;
	if (batio.battinfo.min != UNKNOWN_TIME) {
		bat->h_left = batio.battinfo.min / 60;
		bat->m_left = batio.battinfo.min % 60;
	} else {
		bat->h_left = UNKNOWN_TIME;
		bat->m_left = UNKNOWN_TIME;
	}
	bat->rate = batio.battinfo.rate;
	switch (batio.battinfo.state & ACPI_BATT_STAT_BST_MASK) {
		case 0:
			bat->state = HIGH;
			break;
		case ACPI_BATT_STAT_CHARGING:
			bat->state = CHARGING;
			break;
		case ACPI_BATT_STAT_DISCHARG:
			bat->state = DISCHARCHING;
			break;
		case ACPI_BATT_STAT_CRITICAL:
			bat->state = CRITICAL;
			break;
		case ACPI_BATT_STAT_DISCHARG | ACPI_BATT_STAT_CRITICAL:
			bat->state = CRITICAL_DISCHARGING;
			break;
		case ACPI_BATT_STAT_CHARGING | ACPI_BATT_STAT_CRITICAL:
			bat->state = CRITICAL_CHARGING;
			break;
		default:
			bat->state = UNKNOWN;
	}
	
	// fetch extras if requested
	if (get_extras) {
		// gotta reset the unit since ioctling scrambles it
		batio.unit = bat->num;
		if (ioctl(acpifd, ACPIIO_BATT_GET_BIX, &batio) == -1) {
			// TODO log error
			err(EX_IOERR, "get battery info (%d) failed", bat->num);
			goto fail_return;
		}
		bat->extra.base_unit		= batio.bix.units ? "mA" : "mW";
		bat->extra.design_cap		= batio.bix.dcap;
		bat->extra.last_full_cap	= batio.bix.lfcap;
		bat->extra.design_voltage	= batio.bix.dvol; // caller checks UNKNOWN_CAP
		strlcpy(bat->extra.type, batio.bix.type, ACPI_CMBAT_MAXSTRLEN);
		
		if (ACPI_BIX_REV_MIN_CHECK(batio.bix.rev, ACPI_BIX_REV_0))
			bat->extra.cycles = batio.bix.cycles;
		else {
			// TODO log warning
			fprintf(stderr, "BIX revision too low (%u), needs at least %u.\n",
					batio.bix.rev, ACPI_BIX_REV_0);
		}

		batio.unit = bat->num;
		if (ioctl(acpifd, ACPIIO_BATT_GET_BST, &batio) == -1) {
			// TODO log warning
			err(EX_IOERR, "get battery status (%d) failed", bat->num);
		} else {
			if (batio.bst.state != ACPI_BATT_STAT_NOT_PRESENT)
				bat->extra.current_voltage = batio.bst.volt;
			else {
				// TODO log warning
				fprintf(stderr, "Battery %i not present according to BST.\n",
						bat->num);
			}
		}
		// calc time until full
		if (bat->percent < 100 && bat->state & CHARGING) {
			mins2full = (0.6f*bat->extra.last_full_cap*(100-bat->percent)) /
						(float)bat->rate + 0.5f;
			bat->h_left = mins2full / 60;
			bat->m_left = mins2full % 60;
		}
	}

	close(acpifd);

	bat->valid_read = 1;
	return;

fail_return:
	if (acpifd != -1) close(acpifd);
	return;
}

