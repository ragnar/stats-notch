# TODO

stats:
	clang -Wall -O2 -o stats main.c battery.c ram.c cpu.c thermal_zones.c

notch:
	clang -Wall -O2 -o notch -I. -I/usr/local/include -I/usr/local/include/cairo -I/usr/local/include/pixman-1 -L/usr/local/lib -lwayland-client notch.c

clean:
	rm stats notch

