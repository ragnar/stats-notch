
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "battery.h"
#include "ram.h"
#include "cpu.h"

#define TIME_FMT	"%Y-%m-%d %H:%M"
#define TIME_BUF_SZ	64

int main(int argc, char **argv) {
	int i;

	time_t tm_sec;
	struct tm *local_tm;
	char timebuf[TIME_BUF_SZ];

	struct battery bat;
	struct ram r;
	struct cpu *c;

	time(&tm_sec);
	// TODO check return val
	local_tm = localtime(&tm_sec);
	strftime(timebuf, TIME_BUF_SZ, TIME_FMT, local_tm);
	printf("Time: %s\n", timebuf);

	bat.num = 0;
	battery_info(&bat, true);
	if (bat.valid_read)
		printf("Battery: %d%%, %d:%02d until %scharge rate %d %s\n"
			   "\tDesign/Last full cap: %u/%u %sh\n"
			   "\tDesign/Current volt:  %u/%u mV\n"
			   "\tNumber of cycles:     %u\n",
			   bat.percent, bat.h_left, bat.m_left,
			   bat.state & CHARGING ? "full, " : "empty, dis", bat.rate,
			   bat.extra.base_unit, bat.extra.design_cap,
			   bat.extra.last_full_cap, bat.extra.base_unit,
			   bat.extra.design_voltage, bat.extra.current_voltage,
			   bat.extra.cycles);
	else
		fprintf(stderr, "Fetching battery info failed.\n");
	ram_info(&r);
	printf("RAM usage [GB]:\n"
		   "\twired:    %.2f\n"
		   "\tactive:   %.2f\n"
		   "\tlaundry:  %.2f\n"
		   "\tinactive: %.2f\n"
		   "\tfree:     %.2f\n"
		   "\ttotal:    %.2f\n",
		   BYTES2GB(r.wire), BYTES2GB(r.active), BYTES2GB(r.laundry),
		   BYTES2GB(r.inactive), BYTES2GB(r.free), BYTES2GB(r.tot_mem));

	c = cpu_info(NULL);
//	sleep(1);
	cpu_info(c);
	printf("CPU: %s\n", c->model);
	for (i = 0; i < c->no_cores; i++)
		printf("\tCore %u @ %.2f GHz: %.1f C (tj max: %.1f C, util: %.1f%%)\n",
			   i, (float)c->cores[i].freq / 1000.0f, c->cores[i].temp,
			   c->cores[i].throttle_temp, 100.0f*(1.0f-c->cores[i].util.idle));
	free_cpu_struct(c);

	return 0;
}


