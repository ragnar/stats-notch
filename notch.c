// ref: wayland-book.com, weston examples:
// https://gitlab.freedesktop.org/wayland/weston/-/tree/main/clients and the
// spec: https://wayland.freedesktop.org/docs/html/apa.html
//
// since this is my first wayland gui i'll try to comment/document the shiznaz
// out of it.
#include "wayland-client-protocol.h"
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>	// memfd_create
#include <fcntl.h>		// posix_fallocate
#include <errno.h>		// EINTR
#include <unistd.h>		// close
#include <wayland-client.h>

#ifdef DEBUG
#define LOG_DBG(fmt, args...) printf("[%s:%d] " fmt "\n", \
									 __func__, __LINE__, ##args)
#else
#define LOG_DBG(fmt, args...)
#endif

// this pupper holds all the wayland-hoopla needed to draw pixels on the
// screen. one instance of this structure is used in main which is populated
// in the listener callbacks
struct wayland {
	struct wl_compositor *compositor;
	struct wl_shm *shm;
	struct wl_shm_pool *pool;
	bool supports_alpha;
};

/**************** fwd declaration of listener callbacks ****************/
// see implementation of them for more info.
// registry listeners:
static void reg_handle_global(void *dat, struct wl_registry *reg,
							  uint32_t name, const char *iface, uint32_t ver);
static void reg_handle_global_rm(void *dat, struct wl_registry *reg,
								 uint32_t name);
// shm listener
static void shm_handle_format(void *dat, struct wl_shm *shm, uint32_t fmt);
// anon file creator
static int create_mem_pool(off_t sz);

/**************** listener structs ****************/
static const struct wl_registry_listener reg_listener = {
	.global = reg_handle_global,
	.global_remove = reg_handle_global_rm,
};
static const struct wl_shm_listener shm_listener = {
	.format = shm_handle_format,
};

/**************** listener callback implementations  ****************/
static void reg_handle_global(void *dat, struct wl_registry *reg,
							  uint32_t name, const char *iface, uint32_t ver) {
	// this is where we get the compositor to create our surface, shared memory
	// (wl_shm) to send our pixels to the actual compositor, e.g. hikari, sway,
	// etc, wl_output(s) to add our surfaces to, and finally the layer_shell
	// interface so we can stick our window on top of everything
	struct wayland *s = dat;
	LOG_DBG("interface: '%s', ver: %d, name: %d", iface, ver, name);
	if (strcmp(iface, wl_compositor_interface.name) == 0) {
		// binding, i.e. taking an exisiting object and assigning it an id,
		// the compositor.. we've specified version 4 of wl_compositor. why?
		// becuase that's the latest as of writing, and you know, "newer is
		// always better".
		// used: when we want to create a surface what we do is we ask the
		//       compositor to do it for us (wl_compositor_create_surface)
		s->compositor = wl_registry_bind(reg, name, &wl_compositor_interface, 4);
	}
	else if (strcmp(iface, wl_shm_interface.name) == 0) {
		// version 1 here.. i don't know, i haven't read the full spec of each
		// thing. 1 works, i would assume that there would be some info
		// in the book if it was important.
		// used: when creating a shared memory pool.
		s->shm = wl_registry_bind(reg, name, &wl_shm_interface, 1);
		// we now add a listener to this pupper to check which pixel formats
		// are supported. in theory this shouldn't be needed as ARGBB888 and
		// XRGB8888 are both required, but yeah.
		if (wl_shm_add_listener(s->shm, &shm_listener, s) < 0)
			fprintf(stderr, "wl_shm_add_listener failed\n");
	}
}

static void reg_handle_global_rm(void *dat, struct wl_registry *reg,
								 uint32_t name) {
	LOG_DBG("interface name %d removed\n", name);
}

static void shm_handle_format(void *dat, struct wl_shm *shm, uint32_t fmt) {
	struct wayland *s = dat;
	// TODO handle the other formats..
	if (fmt == WL_SHM_FORMAT_ARGB8888) s->supports_alpha = true;
	LOG_DBG("shm format: 0x%X", fmt);
}

static void cleanup(struct wayland *s) {
	if (s->shm) wl_shm_destroy(s->shm);
	if (s->compositor) wl_compositor_destroy(s->compositor);
}


/**************** other wayland related stuff ****************/
static int create_mem_pool(off_t sz) {
	// since this is a first try we won't be doing anything fancy, just create
	// a memory mapped file - and that will be our pool.
	int fd, ret;
	// so, this is the way the weston simple_shm.c does it. i'm not entirely
	// sure why the file needs to be closed if exec* is called, but it seems
	// reasonable, because mem leaks and whatnot. sealing is to make sure it
	// aint shrunk.
	fd = memfd_create("", MFD_CLOEXEC | MFD_ALLOW_SEALING);
	if (fd >= 0) fcntl(fd, F_ADD_SEALS, F_SEAL_SHRINK);
	else return -1; // errno is set, let caller deal with it
	// the weston example prefers fallocate, again, not 100% sure. assume newer
	// is more better. difference from ftruncate is that is returns error
	// instead of setting errno.
	do { ret = posix_fallocate(fd, 0, sz); } while (ret == EINTR);
	if (ret != 0) {
		close(fd);
		errno = ret;
		return -1;
	}
	return fd;
}

int main(int argc, char *argv[]) {
	int ret;
	struct wayland state = { 0 };
	struct wl_registry *reg;
	struct wl_display *disp = wl_display_connect(NULL);
	// so, the display interface has the object id 1.. based on this we can
	// request the creation of new objects and whatnot. but there's also a
	// bunch of global objects which we'll have a bind using the registry
	if (!disp) {
		fprintf(stderr, "Failed to connect to wayland display.\n");
		return 1;
	}
	// fetch the registry, if you want details see
	// https://wayland-book.com/registry.html
	reg = wl_display_get_registry(disp);
	if (!reg) {
		fprintf(stderr, "Failed to get display registry.\n");
		wl_display_disconnect(disp);
		return 2;
	}
	// the reg listener is fed all the global objects, which includes the
	// wl_compositor (the actual compositor's compositor..?), the shm (shared
	// memory), wl_output, etc.. unfortunately this pupper is undocumented.
	// fan-fucking-tastic, luckily it's just a wrapper for
	// wl_proxy_add_listener, which returns -1 on failure
	ret = wl_registry_add_listener(reg, &reg_listener, &state);
	if (ret < 0) {
		fprintf(stderr, "wl_registry_add_listener failed\n");
		// TODO add jump to err
	}
	// the two roundtrips are very well explained in
	// https://gitlab.freedesktop.org/wayland/weston/-/blob/main/clients/simple-shm.c
	// so check that out!
	ret = wl_display_roundtrip(disp);
	if (ret < 0) {
		fprintf(stderr, "wl_display_roundtrip failed\n");
		// TODO jump to err handler
	}

	// simplest event loop would just be
	// while (wl_display_dispatch(disp) != -1) { }
	// we might need to wl_display_get_fd and then poll as we only want to draw
	// when needed or when we get an event from one of the listeners..
	
	cleanup(&state);
	wl_display_disconnect(disp);
	return 0;
}


